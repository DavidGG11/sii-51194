<<<<<<< HEAD
<<<<<<< HEAD:practica1/src/Mundo.cpp
// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "Mundo.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/file.h>
#include <sys/mman.h>
#include <fcntl.h>
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
//Cerramos tubería

	char salir[100];
	sprintf(salir,"FIN JUEGO");
	write(fd,salir,strlen(salir)+1);
	if(close(fd)==-1)
	{
		perror("ERROR AL CERRAR");
		exit(1);
	}
	p_bot->fin_partida=1;
	munmap(proyeccion,sizeof(bot)); //elimina la proyección en el destructor, sabiendo su tamaño
	printf("Proyección eliminada en Mundo\n");
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;

		//Mensaje de la tuberia ( al usar la función write se guarda en el buffer cadena)

		char cad[200];
		sprintf(cad,"Jugador 2 marca 1 punto, lleva %d", puntos2); //nos permite hacer como un printf pero lo guarda en el buffer
		write(fd,cad,strlen(cad)+1);
		
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;

		//Mensaje de la tubería
		
		char cad[200];
		sprintf(cad,"Jugador 1 marca 1 punto, lleva %d", puntos1); //nos permite hacer como un printf pero lo guarda en el buffer
		write(fd,cad,strlen(cad)+1);
	}

	//PARA EL BOT
	p_bot->esfera=esfera;
 	p_bot->raqueta1=jugador1;
 	
 
 	if (p_bot->accion==1)
 	OnKeyboardDown('w',0,0);
 	else if (p_bot->accion==-1)
  	OnKeyboardDown('s',0,0);
 	else if (p_bot->accion==0);

}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;

	}
}

void CMundo::Init() //pinta las raquetas
{
	//Abrir la tubería en modo escritura
	fd=open("/tmp/loggerfifo",O_WRONLY);
	if(fd==-1)
	{
		perror("Error al abrir fifo");
		exit(1);
	}

//CREACIÓN DEL FICHERO Y PROYECCIÓN
	int file=open("/tmp/datosBot",O_RDWR|O_CREAT|O_TRUNC, 0777);
	if (file==-1)
		{
			printf ("\n Error de apertura del fichero donde vamos a proyectar \n"); 
		}
	else
		{
			printf("Creación de fichero correcta\n");
		}
 	//Asignamos el tamaño del fichero escribiendo en él.
 	write(file,&bot,sizeof(bot));
 	 
 	proyeccion=(char*)mmap(NULL,sizeof(bot),PROT_WRITE|PROT_READ,MAP_SHARED,file,0); //proyectamos el file con tamaño bot
 
 	close(file); //Cerramos el fichero
 	p_bot=(DatosMemCompartida*)proyeccion; 
	p_bot->accion=0;
	p_bot->fin_partida=0;



	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;



}
=======
=======
>>>>>>> practica3
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/file.h>
#include <sys/mman.h>
#include <fcntl.h>

#define MAX 200
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////




CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{

<<<<<<< HEAD
//Cerramos FIFO servidor-cliente
	close(fd_serv);
	unlink("/tmp/FIFOservidor");
<<<<<<< HEAD
	//printf("Fifo servidor-cliente cerrada y eliminada en MundoCliente\n");
//Cerramos FIFO cliente-servidor
	close(fd_cliente);
	unlink("/tmp/FIFOcliente");
	//printf("Fifo cliente-servidor cerrada y eliminada en MundoCliente\n");
=======
//Cerramos FIFO cliente-servidor
	close(fd_cliente);
	unlink("/tmp/FIFOcliente");
>>>>>>> practica3

=======
>>>>>>> practica5
//Cierre de la proyección
	p_bot->fin_partida=1;
	munmap(proyeccion,sizeof(bot)); //elimina la proyección en el destructor, sabiendo su tamaño
<<<<<<< HEAD

=======
>>>>>>> practica3
	unlink("/tmp/datosBot");
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
	}

<<<<<<< HEAD
<<<<<<< HEAD
//LECTURA DE LOS DATOS QUE PROVIENEN DE LA FIFO SERVIDOR-CLIENTE
=======
//Lectura de los datos provenientes a través de la fifo servidor-cliente
>>>>>>> practica3
	char cad[MAX];
	int fd_read;
	fd_read=read(fd_serv,cad, sizeof(cad));
	if (fd_read==-1)
	{
		unlink("/tmp/FIFO_serv");
		printf("Error de lectura del FIFOservidor\n");
		exit(1);
	}
	
	else
<<<<<<< HEAD
	sscanf(cad,"%f %f %f %f %f %f %f %f %f %f %d %d", &esfera.centro.x,&esfera.centro.y,&jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2, &jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2, &puntos1, &puntos2); 
=======
	sscanf(cad,"%f %f %f %f %f %f %f %f %f %f %d %d", &esfera.centro.x,&esfera.centro.y, 		&jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2, &jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2, &puntos1, &puntos2); 
>>>>>>> practica3
=======
	char cad_coord[MAX];
	socket_comunic_sv.Receive(cad_coord,sizeof(cad_coord));
	sscanf(cad_coord,"%f %f %f %f %f %f %f %f %f %f %d %d", &esfera.centro.x,&esfera.centro.y,&jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2, &jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2, &puntos1, &puntos2); 
>>>>>>> practica5



	//PARA EL BOT
	p_bot->esfera=esfera;
 	p_bot->raqueta1=jugador1;
 	
 
 	if (p_bot->accion==1)
 	OnKeyboardDown('w',0,0);
 	else if (p_bot->accion==-1)
  	OnKeyboardDown('s',0,0);
 	else if (p_bot->accion==0);

}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;
	}

	char cad[MAX];
	sprintf(cad,"%c",key);

	//Envío de la tecla mediante el socket
	socket_comunic_sv.Send(cad,sizeof(cad));
}

void CMundo::Init() //pinta las raquetas
{
<<<<<<< HEAD

//CREACIÓN DEL FICHERO Y PROYECCIÓN
	
	munmap(proyeccion,sizeof(bot));
=======
//CREACIÓN DEL FICHERO Y PROYECCIÓN
	
	//munmap(proyeccion,sizeof(bot));
>>>>>>> practica3

	int file=open("/tmp/datosBot",O_RDWR|O_CREAT|O_TRUNC, 0777);
	if (file==-1)
		{
			printf ("\n Error de apertura del fichero donde vamos a proyectar \n"); 
		}
	else
	{			
<<<<<<< HEAD
<<<<<<< HEAD
		printf("\n Se ha creado el fichero datosBot en cliente\n");
=======
		printf("Se ha creado el fichero datosBot");
>>>>>>> practica3
=======
		printf("\nSe ha creado el fichero datosBot en cliente\n");
>>>>>>> practica5
	}

 	//Asignamos el tamaño del fichero escribiendo en él.
 	write(file,&bot,sizeof(bot));
 	 
 	proyeccion=(char*)mmap(NULL,sizeof(bot),PROT_WRITE|PROT_READ,MAP_SHARED,file,0); //proyectamos el file con tamaño bot
 
 	close(file); //Cerramos el fichero
 	p_bot=(DatosMemCompartida*)proyeccion; 
	p_bot->accion=0;
	p_bot->fin_partida=0;

<<<<<<< HEAD
<<<<<<< HEAD
//CREACIÓN FIFO servidor-cliente
	unlink("/tmp/FIFOservidor"); //la eliminamos por si ya está creada	

	int fd_aux;
	fd_aux=mkfifo("/tmp/FIFOservidor",0666);
	if(fd_aux==-1)
	{
		perror("Error al crear la fifo servidor-cliente en MundoCliente");
	}

//CREACIÓN DE FIFO CLIENTE-SERVIDOR
	unlink("/tmp/FIFOcliente"); //eliminamos por si ya está creado 

	int aux_fd;
	aux_fd=mkfifo("/tmp/FIFOcliente",0666);
	if(aux_fd==-1)
	{
		perror("No se ha podido crear la fifo cliente-servidor o ya existe");
	}
//APERTURA DE FIFO SERVIDOR-CLIENTE (SOLO LECTURA)

	fd_serv=open("/tmp/FIFOservidor",O_RDONLY);
	if(fd_serv==-1)
	{
		perror("Error al abrir la fifo servidor-cliente en MundoCliente\n");
	}
	else
		printf("FIFO Servidor-cliente creada y abierta\n");


//APERTURA DE LA FIFO CLIENTE-SERVIDOR (SOLO LECTURA)
=======
//COMUNICACIÓN CON SOCKETS
	char nombre[MAX];
	printf("\nIntroduzca su nombre de cliente:\n");
	gets(nombre);
>>>>>>> practica5
	
	socket_comunic_sv.Connect((char *)"127.0.0.01",2500);
	socket_comunic_sv.Send(nombre,strlen(nombre)+1);


	Plano p;
//Esfera inicialmente
	esfera.centro.x=0;
	esfera.centro.y=0;
=======


	Plano p;
>>>>>>> practica3
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

<<<<<<< HEAD


}
>>>>>>> practica4:practica1/src/MundoCliente.cpp
=======
//CREACIÓN FIFO servidor-cliente
	unlink("/tmp/FIFOservidor"); //la eliminamos por si ya está creada	

	int fd_aux;
	fd_aux=mkfifo("/tmp/FIFOservidor",0666);
	if(fd_aux==-1)
	{
		perror("Error al crear la fifo servidor-cliente en MundoCliente");
	}

//APERTURA DE FIFO SERVIDOR-CLIENTE (SOLO LECTURA)

	fd_serv=open("/tmp/FIFOservidor",O_RDONLY);
	if(fd_serv==-1)
	{
		perror("Error al abrir la fifo servidor-cliente en MundoCliente");
	}
	else
		printf("FIFO Servidor-cliente creada y abierta");


//CREACIÓN DE FIFO CLIENTE-SERVIDOR
	unlink("/tmp/FIFOcliente"); //eliminamos por si ya está creado 

	int aux_fd;
	aux_fd=mkfifo("/tmp/FIFOcliente",0666);
	if(aux_fd==-1)
	{
		perror("No se ha podido crear la fifo cliente-servidor o ya existe");
	}

//APERTURA DE LA FIFO CLIENTE-SERVIDOR (SOLO LECTURA)
	
	fd_cliente=open("/tmp/FIFOcliente",O_RDONLY);
	if(fd_cliente==-1)
	{
		perror("No se ha podido abrir la fifo cliente-servidor");
	}
	else
		printf("FIFO cliente-servidor creada y abierta");



}
>>>>>>> practica3
