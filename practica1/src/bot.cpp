 #include <iostream>
 #include <sys/types.h>
 #include <sys/stat.h>
 #include <sys/mman.h>
 #include <fcntl.h>
 #include <unistd.h>
 #include "DatosMemCompartida.h" //Recordamos que aquí está el redireccionamiento a raqueta y esfera

int main() {
	
	int file;
	char *proyeccion;
	DatosMemCompartida*p_bot;
	
	//Abrimos el archivo que ya hemos creado en mundo.cpp, será el mecanismo de comunicación
	file=open("/tmp/datosBot",O_RDWR); //No hacen falta los bits de permiso ya que se el fichero solo se abre, se crea en mundo.cpp
	if (file==-1)
	{
    		 perror ("Error abriendo fichero bot");
    		 return -1;
	}

	//Proyección del fichero
	proyeccion=(char*)mmap(NULL,sizeof(p_bot),PROT_WRITE|PROT_READ,MAP_SHARED,file,0);
   	if (proyeccion==MAP_FAILED)
	{
		perror("No puede abrirse el fichero que proyectamos"); 
	     	return -1;
	}
 	
	//Cerramos descriptor
	close(file);
	
	//Asignamos dirección de comienzo de la región creada al atributo de tipo puntero
	p_bot=(DatosMemCompartida*)proyeccion;

	 //Movimiento de la raqueta
     	while(1)
     	{
		float centro_Raqueta;
		centro_Raqueta=((p_bot->raqueta1.y1+p_bot->raqueta1.y2)/2);
		printf("Entrando bucle\n");
		 
		if(centro_Raqueta<p_bot->esfera.centro.y)
		    p_bot->accion=1; //subir
	 
		else if(centro_Raqueta>p_bot->esfera.centro.y)
		     p_bot->accion=-1; //bajar
	 
		else
		    p_bot->accion=0;

	       	if (p_bot->fin_partida) break;
		
		usleep(25000);
		printf("Saliendo bucle\n");
	}
	munmap(proyeccion,sizeof(p_bot));
     	return 0;
     }
