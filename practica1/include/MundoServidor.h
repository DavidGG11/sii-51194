#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_


#include <vector>
#include "Plano.h"
#include "Socket.h"

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <pthread.h>

#include "DatosMemCompartida.h" //Aquí ya están Esfera y Raqueta

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CMundo  
{
public:
	void Init();
	CMundo();
	virtual ~CMundo();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();
	void RecibeComandosJugador();

	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;

	int puntos1;
	int puntos2;
	
	int fd_logger;
	//int fd_serv; //descriptor de la fifo servidor-cliente
	//int fd_cliente;

	Socket socket_comunic_cl;
	Socket socket_conex_cl;

	pthread_t thd;

	//Para el bot
	DatosMemCompartida *p_bot;
	DatosMemCompartida bot;
	char*proyeccion;
	
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
